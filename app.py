from flask import Flask, request
from flask.json import jsonify
import json

app = Flask(__name__)

@app.route('/process', methods=['POST'])
def processjson():
	data = request.get_json()
	name = data['device']
	return jsonify({'device' : name})
